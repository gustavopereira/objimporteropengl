package br.pucpr.cg;

import br.pucpr.mage.*;
import br.pucpr.mage.PBRMaterial;
import br.pucpr.mage.Window;
import br.pucpr.mage.normals.NormalsMaterial;
import br.pucpr.mage.phong.DirectionalLight;
import org.joml.Matrix4f;
import org.joml.Vector3f;


import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.GL_TEXTURE_CUBE_MAP;

public class ObjModelViewer implements Scene {

    private static final String PATH = "resources/";
    private Keyboard keys = Keyboard.getInstance();

    private MovableCamera camera = new MovableCamera();
    private float lookX = 0.0f;
    private float rotation = 0.0f;

    private DirectionalLight light;

    private Mesh mesh;
    private PBRMaterial material;
    private NormalsMaterial normalsMaterial;


    //Hotkeys
    public int toggleNormalDebug = GLFW_KEY_X;

    public int toggleAutoRotate = GLFW_KEY_P;

    public int rotateLightLeft = GLFW_KEY_J;
    public int rotateLightRight = GLFW_KEY_K;

    public int rotateModelLeft = GLFW_KEY_N;
    public int rotateModelRight = GLFW_KEY_M;

    //Visualization Settings
    private boolean showNormals = false;
    private final float autoRotateSpeed = .5f;
    private boolean autoRotate = true;

    @Override
    public void init() {
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_CULL_FACE);
        glEnable(GL_TEXTURE_CUBE_MAP);

        glPolygonMode(GL_FRONT_FACE, GL_LINE);
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

        camera.getPosition().z = -1.2f;

        light = new DirectionalLight(
                new Vector3f(1.0f, -1.0f, 1.0f),    //direction
                new Vector3f(0.1f, 0.1f, 0.1f),    //ambient
                new Vector3f(1.0f, 1.0f, 1.0f),    //diffuse
                new Vector3f(1.0f, 1.0f, 1.0f));   //specular

        String fileName = "Lion/LionStatue";

        mesh = MeshFactory.CreateImportedMesh(MeshData.loadObj(fileName));
        Texture lionAlbedo = new Texture(PATH + fileName + "_D.jpg");
        Texture lionNormal = new Texture(PATH + fileName + "_N.jpg");
        Texture lionSpecular = new Texture(PATH + fileName + "_S.jpg");
        Texture lionAO = new Texture(PATH + fileName + "_AO.jpg");

        normalsMaterial = new NormalsMaterial();

        material = new PBRMaterial(
                new Vector3f(1.0f, 1.0f, 1.0f), //ambient
                new Vector3f(0.7f, 0.7f, 0.7f), //diffuse
                new Vector3f(0.5f, 0.5f, 0.5f), //specular
                128.0f);                         //specular power

        material.setTexture(lionAlbedo);
        material.setNormal(lionNormal);
        material.setSpecular(lionSpecular);
        material.setOcclusion(lionAO);
    }


    @Override
    public void update(float secs) {
        if (keys.isPressed(GLFW_KEY_ESCAPE)) {
            glfwSetWindowShouldClose(glfwGetCurrentContext(), GLFW_TRUE);
            return;
        }

        camera.update(keys, secs, 1);

        if (keys.isPressed(toggleNormalDebug)) showNormals = !showNormals;

        if (keys.isPressed(toggleAutoRotate)) autoRotate = !autoRotate;

        if (autoRotate) {
            rotation += secs * autoRotateSpeed;
            light.getDirection().x = -5;
            return;
        } else {
            if (keys.isDown(rotateModelLeft)) rotation += secs;
            else if (keys.isDown(rotateModelRight)) rotation -= secs;
        }

        if (keys.isDown(rotateLightLeft)) lookX -= 10 * secs;
        else if (keys.isDown(rotateLightRight)) lookX += 10 * secs;

        light.getDirection().x = lookX;
    }

    private void drawNormals(Mesh mesh) {
        if (!showNormals) return;

        Shader shader = normalsMaterial.getShader();
        shader.bind()
                .setUniform("uProjection", camera.getProjectionMatrix())
                .setUniform("uView", camera.getViewMatrix())
                .setUniform("uCameraPosition", camera.getPosition());
        shader.unbind();

        mesh.draw(normalsMaterial);
    }

    private void drawMesh(Mesh mesh, Material mat) {
        Shader shader = mat.getShader();
        shader.bind()
                .setUniform("uProjection", camera.getProjectionMatrix())
                .setUniform("uView", camera.getViewMatrix())
                .setUniform("uCameraPosition", camera.getPosition())
                .setUniform("uReflectivity", 1);
        light.apply(shader);
        shader.unbind();

        mesh.setUniform("uWorld", new Matrix4f().rotateY(rotation));
        mesh.draw(mat);

        drawNormals(mesh);
    }

    @Override
    public void draw() {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        drawMesh(mesh, material);
    }

    @Override
    public void deinit() {

    }

    public static void main(String[] args) {
        new Window(new ObjModelViewer(), "OBJ Model Viewer", 1366, 768).show();
    }
}
