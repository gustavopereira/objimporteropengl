package br.pucpr.mage;

import org.joml.Vector3f;

import java.util.HashMap;
import java.util.Map;

import static org.lwjgl.opengl.GL13.GL_TEXTURE0;
import static org.lwjgl.opengl.GL13.glActiveTexture;

public class PBRMaterial extends SimpleMaterial {
    private static final String PATH = "/br/pucpr/mage/resource/pbr/";

    private Vector3f ambientColor;
    private Vector3f diffuseColor;
    private Vector3f specularColor;
    private float specularPower;
    private Map<String, Texture> textures = new HashMap<>();

    private Shader shader = Shader.loadProgram(PATH + "pbr");

    public PBRMaterial(Vector3f ambientColor, Vector3f diffuseColor, Vector3f specularColor, float specularPower) {
        super(PATH + "pbr.vert", PATH + "pbr.frag");

        this.ambientColor = ambientColor;
        this.diffuseColor = diffuseColor;
        this.specularColor = specularColor;
        this.specularPower = specularPower;
    }

    public PBRMaterial setNormal(Texture normalMap) {
        return setTexture("uNormal", normalMap);
    }

    public Vector3f getAmbientColor() {
        return ambientColor;
    }
    public Vector3f getDiffuseColor() {
        return diffuseColor;
    }
    public Vector3f getSpecularColor() {
        return specularColor;
    }
    public float getSpecularPower() {
        return specularPower;
    }

    public void setSpecularPower(float specularPower) {
        this.specularPower = specularPower;
    }    
    
    public Material setTexture(Texture texture) {
        return setTexture("uMainTex", texture);
    }

    public PBRMaterial setTexture(String name, Texture texture) {
        if (texture == null) {
            textures.remove(name);
        } else {
            textures.put(name, texture);
        }
        return this;
    }

    @Override
    public void apply() {
        shader.setUniform("uAmbientMaterial", ambientColor);
        shader.setUniform("uDiffuseMaterial", diffuseColor);
        shader.setUniform("uSpecularMaterial", specularColor);
        shader.setUniform("uSpecularPower", specularPower);
        
        int texCount = 0;        
        for (Map.Entry<String, Texture> entry : textures.entrySet()) {
            glActiveTexture(GL_TEXTURE0 + texCount);
            entry.getValue().bind();
            shader.setUniform(entry.getKey(), texCount);
            texCount = texCount + 1;            
        }
    }

    @Override
    public Shader getShader() {
        return shader;
    }

    @Override
    public void setShader(Shader shader) {
        if (shader == null) {
            throw new IllegalArgumentException("Shader cannot be null!");
        }
        this.shader = shader;
    }

    public PBRMaterial setOcclusion(Texture occlusion) {
        return setTexture("uOcclusion", occlusion);
    }

    public PBRMaterial setSpecular(Texture specular) {
        return setTexture("uSpecular", specular);
    }
}
