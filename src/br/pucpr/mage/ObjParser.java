package br.pucpr.mage;

import org.joml.Vector2f;
import org.joml.Vector3f;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class ObjParser {
    private static final String VERTEX_ID = "v ";
    private static final String UV_ID = "vt ";
    private static final String NORMAL_ID = "vn ";
    private static final String OBJFACE_ID = "f ";

    private static ArrayList<Vector3f> VertexBuffer;
    private static ArrayList<Vector2f> UvBuffer;
    private static ArrayList<Vector3f> NormalBuffer;

//    private void Start()
//    {
//        var path = Application.streamingAssetsPath + "/" + FileName;
//        var objLines = GetObj(path);
//
//        MeshData = GetMesh(objLines);
//
//        var meshFilter = gameObject.GetComponent<MeshFilter>();
//
//        var mesh = new Mesh
//        {
//            vertices = MeshData.Vertices,
//                    normals = MeshData.Normals,
//                    uv = MeshData.UVs,
//                    triangles = MeshData.Triangles
//        };
//
//        mesh.RecalculateTangents();
//
//        meshFilter.sharedMesh = mesh;
//    }


    private static void GetVertices(List<String> objLines) {
        if (VertexBuffer == null) VertexBuffer = new ArrayList<Vector3f>();
        else VertexBuffer.clear();

        int idLenght = VERTEX_ID.length();

        for (String line : objLines) {
            if (line.equals("") || line.substring(0, 1).equals("#")) continue;

            if (line.substring(0, idLenght).equals(VERTEX_ID)) // Vertex Found
            {
                String vertexesStr = line.substring(idLenght, idLenght + (line.length() - idLenght)); //Remove - 'v '
                String[] coords = vertexesStr.split(" "); //Remove Spaces

                String xStr = coords[0];
                String yStr = coords[1];
                String zStr = coords[2];

                float x = Float.parseFloat(xStr);
                float y = Float.parseFloat(yStr);
                float z = Float.parseFloat(zStr);

                VertexBuffer.add(new Vector3f(x, y, z));
            }
        }
    }

    private static void GetUVs(List<String> objLines) {
        if (UvBuffer == null) UvBuffer = new ArrayList<Vector2f>();
        else UvBuffer.clear();

        int idLenght = UV_ID.length();

        for (String line : objLines) {
            if (line.equals("") || line.substring(0, 1).equals("#")) continue;

            if (line.substring(0, idLenght).equals(UV_ID)) // uv coord found
            {
                String vertexesStr = line.substring(idLenght); //Remove - 'vt '
                String[] coords = vertexesStr.split(" "); //Remove Spaces

                String xStr = coords[0];
                String yStr = coords[1];

                float x = Float.parseFloat(xStr);
                float y = Float.parseFloat(yStr);

                UvBuffer.add(new Vector2f(x, -y));
            }
        }
    }

    private static void GetNormals(List<String> objLines) {
        if (NormalBuffer == null) NormalBuffer = new ArrayList<Vector3f>();
        else NormalBuffer.clear();

        int idLenght = NORMAL_ID.length();

        for (String line : objLines) {
            if (line.equals("") || line.substring(0, 1).equals("#")) continue;

            if (line.substring(0, idLenght).equals(NORMAL_ID)) // Vertex Found
            {
                String vertexesStr = line.substring(idLenght, idLenght + (line.length() - idLenght)); //Remove - 'v '
                String[] coords = vertexesStr.split(" "); //Remove Spaces

                String xStr = coords[0];
                String yStr = coords[1];
                String zStr = coords[2];

                float x = Float.parseFloat(xStr);
                float y = Float.parseFloat(yStr);
                float z = Float.parseFloat(zStr);

                NormalBuffer.add(new Vector3f(x, y, z));
            }
        }
    }

    public static MeshData GetMesh(List<String> objLines) {
        int idLenght = OBJFACE_ID.length();

        GetVertices(objLines);    //AddToBuffer
        GetUVs(objLines);         //AddToBuffer
        GetNormals(objLines);     //AddToBuffer

        ArrayList vertices = new ArrayList<Vector3f>();
        ArrayList uvs = new ArrayList<Vector2f>();
        ArrayList normals = new ArrayList<Vector3f>();
        ArrayList triangles = new ArrayList<Integer>();

        for (String line : objLines) {
            if (line.equals("") || line.substring(0, 1).equals("#")) continue;

            if (line.substring(0, idLenght).equals(OBJFACE_ID)) {
                String vertexesStr = line.substring(idLenght, idLenght + (line.length() - idLenght));
                String[] coords = vertexesStr.split(" ");

                String[] coord0 = coords[0].split("/");
                String[] coord1 = coords[1].split("/");
                String[] coord2 = coords[2].split("/");

                OBJFace face = new OBJFace();

                face.A.VertexIndex = (Integer.parseInt(coord0[0])) - 1;
                face.A.UvIndex = (Integer.parseInt(coord0[1]) - 1);
                face.A.NormalIndex = (Integer.parseInt(coord0[2]) - 1);

                face.B.VertexIndex = (Integer.parseInt(coord1[0]) - 1);
                face.B.UvIndex = (Integer.parseInt(coord1[1]) - 1);
                face.B.NormalIndex = (Integer.parseInt(coord1[2]) - 1);

                face.C.VertexIndex = (Integer.parseInt(coord2[0]) - 1);
                face.C.UvIndex = (Integer.parseInt(coord2[1]) - 1);
                face.C.NormalIndex = (Integer.parseInt(coord2[2]) - 1);


                //Vertices
                vertices.add(VertexBuffer.get(face.A.VertexIndex));
                vertices.add(VertexBuffer.get(face.B.VertexIndex));
                vertices.add(VertexBuffer.get(face.C.VertexIndex));

                //Uvs
                uvs.add(UvBuffer.get(face.A.UvIndex));
                uvs.add(UvBuffer.get(face.B.UvIndex));
                uvs.add(UvBuffer.get(face.C.UvIndex));

                //Normals
                normals.add(NormalBuffer.get(face.A.NormalIndex));
                normals.add(NormalBuffer.get(face.B.NormalIndex));
                normals.add(NormalBuffer.get(face.C.NormalIndex));

                //Triangles
                int trisCount = triangles.size();

                triangles.add(0 + trisCount);
                triangles.add(1 + trisCount);
                triangles.add(2 + trisCount);
            }
        }

        return new MeshData(vertices, uvs, normals, triangles);
    }
}