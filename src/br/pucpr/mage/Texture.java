package br.pucpr.mage;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GL13;

import java.awt.image.BufferedImage;
import java.nio.ByteBuffer;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.GL_TEXTURE0;
import static org.lwjgl.opengl.GL13.GL_TEXTURE_CUBE_MAP;
import static org.lwjgl.opengl.GL13.glActiveTexture;
import static org.lwjgl.opengl.GL30.glGenerateMipmap;

public class Texture {
    private int id;
    private TextureParameters parameters;

    public Texture(int width, int height, TextureParameters parameters) {        
        this.parameters = parameters;
        
        id = glGenTextures();
        glBindTexture(GL_TEXTURE_2D, id);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 
                width, height, 0,  
                GL_RGBA, GL_UNSIGNED_BYTE, 0);
        parameters.apply(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, 0);        
    }


    public int Cubemap(String path){
        int id = glGenTextures();

        glBindTexture(GL_TEXTURE_CUBE_MAP, id);

        // Carregar 6 Imagens
        Image posX = new Image(path);
        Image negX = new Image(path);
        Image posY = new Image(path);
        Image negY = new Image(path);
        Image posZ = new Image(path);
        Image negZ = new Image(path);

        GL11.glTexParameteri(GL13.GL_TEXTURE_CUBE_MAP, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
        GL11.glTexParameteri(GL13.GL_TEXTURE_CUBE_MAP, GL12.GL_TEXTURE_WRAP_R, GL12.GL_CLAMP_TO_EDGE);
        GL11.glTexParameteri(GL13.GL_TEXTURE_CUBE_MAP, GL11.GL_TEXTURE_WRAP_S, GL12.GL_CLAMP_TO_EDGE);
        GL11.glTexParameteri(GL13.GL_TEXTURE_CUBE_MAP, GL11.GL_TEXTURE_WRAP_T, GL12.GL_CLAMP_TO_EDGE);

        GL11.glTexImage2D(GL13.GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, GL11.GL_RGBA, posX.getWidth(), posX.getHeight(), 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, posX.getPixels());
        GL11.glTexImage2D(GL13.GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, GL11.GL_RGBA, negX.getWidth(), negX.getHeight(), 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, negX.getPixels());
        GL11.glTexImage2D(GL13.GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, GL11.GL_RGBA, posY.getWidth(), posY.getHeight(), 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, posY.getPixels());
        GL11.glTexImage2D(GL13.GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, GL11.GL_RGBA, negY.getWidth(), negY.getHeight(), 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, negY.getPixels());
        GL11.glTexImage2D(GL13.GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, GL11.GL_RGBA, posZ.getWidth(), posZ.getHeight(), 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, posZ.getPixels());
        GL11.glTexImage2D(GL13.GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, GL11.GL_RGBA, negZ.getWidth(), negZ.getHeight(), 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, negZ.getPixels());

        return id;
    }

    public Texture(Image image, TextureParameters parameters) {
        //Valida os parametros
        if (image.getChannels() < 3) {
            throw new IllegalArgumentException("Image must be RGB or RGBA!");
        }
        if (parameters == null) {
            throw new IllegalArgumentException("Parameters can't be null!");
        }

        int format = image.getChannels() == 3 ? GL_RGB : GL_RGBA;
        
        //Copia os dados        
        id = glGenTextures();        
        glBindTexture(GL_TEXTURE_2D, id);        
        glTexImage2D(GL_TEXTURE_2D, 0, format, 
                image.getWidth(), image.getHeight(), 0,  
                format, GL_UNSIGNED_BYTE, image.getPixels());
        
        //Ajuste dos parametros
        this.parameters = parameters;
        parameters.apply(GL_TEXTURE_2D);
        if (parameters.isMipMapped()) {
            glGenerateMipmap(GL_TEXTURE_2D);
        }
        
        //Limpeza
        glBindTexture(GL_TEXTURE_2D, 0);
    }

    public Texture(Image image) {
        this(image, new TextureParameters());
    }
    
    public Texture(String imagePath, TextureParameters params) {
        this(new Image(imagePath), params);
    }
    
    public Texture(String imagePath) {
        this(new Image(imagePath));
    }
    
    public int getId() {
        return id;
    }
    
    public TextureParameters getParameters() {
        return parameters;
    }
    
    public Texture bind() {
        glBindTexture(GL_TEXTURE_2D, id);
        return this;
    }

    public Texture unbind() {
        glBindTexture(GL_TEXTURE_2D, 0);
        return this;
    }
}
